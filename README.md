# legalmd

An extension of Markdown to notate Australian-style legal markup, using the Python [mistletoe](https://github.com/miyuchina/mistletoe) Markdown parser.

This project has moved to https://yingtongli.me/git/legalmd/
